<div class="row">

	<div class="col-md-12">

		<div class="card">
		  <div class="card-body">
		    <h5 class="card-title">Cadastro de Usuário</h5>
		    <p class="card-text"><sup>Preencha os campos abaixo.</sup></p>

		    <form method="post" action="">

		    	<div class="form-group">
		    		<label>Nome do Aluno</label>
		    		<input type="text" name="nome" class="form-control" required>
		    	</div>

		    	<div class="form-group">
		    		<label>Número de Telefone</label>
		    		<input type="text" name="celular" class="form-control" required>
		    	</div>

		    	<div class="form-group">
		    		<button class="btn btn-success" name="cadastrar">Cadastrar</button>
		    	</div>

		    </form>

		    <?php

		    if(isset($_POST['cadastrar']))
		    {
		    	$nome = $_POST['nome'];
		    	$celular = $_POST['celular'];

		    	$sub = ['.', '/', '(', ')', '-', ' '];

		    	$celular = substr(str_replace($sub, '', $celular),2);

		    	$usuario = $con->prepare("SELECT * FROM usuario WHERE celular = :celular");
		    	$usuario->bindValue(":celular", $celular);
		    	$usuario->execute();

		    	if($usuario->rowCount() > 0)
		    		alert('Usuário já cadastrado!', '');

		    	$sql = $con->prepare("INSERT INTO usuario (celular, nome) VALUES ( :celular , :nome )");
		    	$sql->bindValue(":celular", $celular);
		    	$sql->bindValue(":nome", $nome);
		    	$sql->execute();

		    	echo "<script>alert('Cadastrado com sucesso!');location='?page=cadastro';</script>";

		    }

		    ?>

		  </div>
		</div>

	</div>	

</div>