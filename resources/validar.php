<div class="row">

	<div class="col-md-12">

		<div class="card">
		  <div class="card-body">
		    <h5 class="card-title">Validar Usuário</h5>
		    <p class="card-text"><sup>Preencha os campos abaixo.</sup></p>

		    <form method="post" action="">

		    	<div class="form-group">
		    		<label>Número de Telefone</label>
		    		<input type="text" name="celular" class="form-control" required>
		    	</div>

		    	<div class="form-group">
		    		<label>Código</label>
		    		<input type="text" name="codigo" class="form-control" required>
		    	</div>

		    	<div class="form-group">
		    		<button class="btn btn-success" name="validar">Validar</button>
		    	</div>

		    </form>

		    <?php

		    if(isset($_POST['validar']))
		    {
		    	$celular = $_POST['celular'];
		    	$replace = ['.', '-', '(', ')', ' '];
		    	$code = $_POST['codigo'];

		    	$celular = substr(str_replace($replace, '', $celular),2);

		    	$usuario = $con->prepare("SELECT * FROM usuario WHERE celular = :celular LIMIT 1");
		    	$usuario->bindValue(":celular", $celular);
		    	$usuario->execute();

		    	$codigo = $con->prepare("SELECT * FROM code WHERE code = :code LIMIT 1");
		    	$codigo->bindValue(":code", $code);
		    	$codigo->execute();

		    	if(!$codigo->rowCount() > 0)
		    		alert('Credencial inválida!', '');

		    	if($usuario->rowCount() > 0)
		    	{

		    		$avaliacao = $con->prepare("SELECT * FROM avaliacao WHERE id_usuario = :celular LIMIT 1");
		    		$avaliacao->bindValue(':celular', $celular);
		    		$avaliacao->execute();

		    		if(!$avaliacao->rowCount() > 0)
		    		{
		    			$usuario = $usuario->fetch(PDO::FETCH_OBJ);
		    			$_SESSION['user_id'] = $usuario->celular;
		    			alert('Usuário liberado para responder!', 'index.php');
		    		}
		    		else
		    		{
		    			alert('Usuário já respondeu ao questionário!', '');
		    		}

		    	}

		    	alert('Usuário liberado para cadastro!', '?page=cadastro');

		    }

		    ?>

		  </div>
		</div>

	</div>	

</div>