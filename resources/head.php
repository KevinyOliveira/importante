<!DOCTYPE html>
<html>
<head>
	<title><?=env('TITLE')?></title>
	<meta charset="Unicode,UTF-8">
	<link rel="stylesheet" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" href="resources/css/estilo.css" type="text/css">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>

<body>

	<div class="navbar">

		<div class="row">
			
			<div class="col-md-6 text-left">
				<a href="index.php">
					<img src="resources/images/logotipo.jpeg" class="img_rendering">
				</a>
			</div>

			<a class="btn btn-danger cadastro" href="?page=cadastro">Cadastrar</a>
		
		</div>

	</div>

	<div class="container">

		<div class="row">

			<div class="col-md-12">