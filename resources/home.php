<?php
	
	if(!isset($_SESSION['user_id']))
		echo "<script>location='?page=validar';</script>";

?>

<div class="row">

	<form method="post" action="">

		<div class="col-md-12">

			<?php
				$i = 1;
				$questoes = $con->prepare("SELECT * FROM questao");
				$questoes->execute();

				while($questao = $questoes->fetch(PDO::FETCH_OBJ))
				{
			?>

				<div class="card">
				  <div class="card-body">
				    <h5 class="card-title"><?=$questao->descricao?></h5>
				    <p class="card-text"><sup>Escolha uma das opções abaixo.</sup></p>

				    <div class="row">

					    <?php

					    	$equipes = $con->prepare("SELECT * FROM equipe");
					    	$equipes->execute();

					    	while($equipe = $equipes->fetch(PDO::FETCH_OBJ))
					    	{
					    ?>

					    	<div class="col-md-12">
							    <div class="form-group">
							    	<input type="radio" id="questao<?=$i?>" value="<?=$equipe->id_equipe?>" name="<?=$questao->id_questao?>" required> <?=$equipe->produto?>
							    </div>
							</div>

						<?php } ?>

					</div>

				  </div>
				</div>

			<?php 
			$i++;
			}
			?>

			<br>

			<div class="form-group">
				<button class="btn btn-success" name="finalizar" type="submit">Finalizar</button>
			</div>

			<div class="form-group">
				<a href="?cancelar" class="btn btn-danger">Cancelar</a>
			</div>

		</div>

	</form>

	<?php

	if(isset($_POST['finalizar']))
	{

		$questoes = $con->prepare("SELECT * FROM questao");
		$questoes->execute();

		while($questao = $questoes->fetch(PDO::FETCH_OBJ))
		{
			$id_equipe = $_POST[$questao->id_questao];

			$save = $con->prepare("INSERT INTO avaliacao (id_questao, id_usuario, id_equipe, dia_horario) VALUES ( :id_questao , :id_usuario , :id_equipe , :dia_horario )");
			$save->bindValue(":id_questao", $questao->id_questao);
			$save->bindValue(":id_usuario", $_SESSION['user_id']);
			$save->bindValue(":id_equipe", $id_equipe);
			$save->bindValue(":dia_horario", date('Y-m-d H:i:s'));
			$save->execute();
		}

		unset($_SESSION['user_id']);
		alert('Finalizado com sucesso! Obrigado.', '');

	}

	if(isset($_GET['cancelar']))
	{

		unset($_SESSION['user_id']);
		redirect('?page=validar');

	}

	?>

</div>
