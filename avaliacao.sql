-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 23-Out-2018 às 18:11
-- Versão do servidor: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `avaliacao`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao`
--

DROP TABLE IF EXISTS `avaliacao`;
CREATE TABLE IF NOT EXISTS `avaliacao` (
  `id_avaliacao` int(11) NOT NULL AUTO_INCREMENT,
  `id_questao` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_equipe` int(11) NOT NULL,
  `dia_horario` datetime DEFAULT NULL,
  PRIMARY KEY (`id_avaliacao`,`id_questao`,`id_usuario`,`id_equipe`),
  KEY `fk_questao` (`id_questao`),
  KEY `fk_usuario` (`id_usuario`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `avaliacao`
--

INSERT INTO `avaliacao` (`id_avaliacao`, `id_questao`, `id_usuario`, `id_equipe`, `dia_horario`) VALUES
(16, 4, 967187736, 7, '2018-10-23 17:14:22'),
(15, 3, 967187736, 1, '2018-10-23 17:14:22'),
(14, 2, 967187736, 4, '2018-10-23 17:14:22'),
(13, 1, 967187736, 2, '2018-10-23 17:14:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `code`
--

DROP TABLE IF EXISTS `code`;
CREATE TABLE IF NOT EXISTS `code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `equipe`
--

DROP TABLE IF EXISTS `equipe`;
CREATE TABLE IF NOT EXISTS `equipe` (
  `id_equipe` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `produto` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_equipe`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `equipe`
--

INSERT INTO `equipe` (`id_equipe`, `nome`, `produto`) VALUES
(1, '2ADM', 'Pop cake - Bolo de chocolate c/ cobertura de chocolate branco e tradicional'),
(2, '2ADM', 'Pop cake - Bolo de limão c/ cobertura de chocolate branco e tradicional'),
(3, '2ADM', 'Pop cake - Bolo de coco c/ cobertura de beijinho'),
(4, '2ADM', 'Brigadeiro De Ricota'),
(5, '1ADM', 'CupCake de Cenoura com Brigadeiro'),
(6, '2LOG', 'Pipoca Gourmet'),
(7, '1LOG', 'Patê de Mortadela e Alho');

-- --------------------------------------------------------

--
-- Estrutura da tabela `questao`
--

DROP TABLE IF EXISTS `questao`;
CREATE TABLE IF NOT EXISTS `questao` (
  `id_questao` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_questao`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `questao`
--

INSERT INTO `questao` (`id_questao`, `descricao`) VALUES
(1, 'INOVAÇÃO'),
(2, 'SABOR'),
(3, 'QUALIDADE'),
(4, 'ATENDIMENTO DA EQUIPE');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `celular` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`celular`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`celular`, `nome`) VALUES
(962445494, 'Sueli Barioni'),
(967187736, 'Daniel Bruno');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
