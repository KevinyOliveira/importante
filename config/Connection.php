<?php

namespace Config;

use PDO;

class Connection {

	protected $host;
	protected $database;
	protected $user;
	protected $password;

	public function __construct()
	{

		$this->host = env('DB_HOST');
		$this->database = env('DB_DATABASE');
		$this->user = env('DB_USER');
		$this->password = env('DB_PASS');

	}

	public function connect()
	{

		$host = $this->host;
		$database = $this->database;
		$user = $this->user;
		$password = $this->password;

		$connection = new PDO('mysql:host='.$host.';dbname='.$database, $user, $password);

		return $connection;

	}

}