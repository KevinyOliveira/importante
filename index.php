<?php
session_start();

//READ FILES

$paths = [
	__DIR__.'/app/',
	__DIR__.'/app/model/',
	__DIR__.'/app/helper/',
	__DIR__.'/config/'
];

foreach($paths as $path)
{

	@$dir_app = opendir($path);

	$active = ['php'];

	while(@$archive = readdir($dir_app))
	{
		@$extension = end(explode('.', $archive));

		if(in_array($extension, $active))
			include $path.$archive;

	}

}

//USE

use Config\Connection;

//CONNECTION

$connect = new Connection();
$con = $connect->connect();

//HEAD
include(__DIR__.'/resources/head.php');

//BODY
if(isset($_GET['page']))
{
	include(__DIR__.'/resources/'.$_GET["page"].'.php');
}
else
{
	include(__DIR__.'/resources/home.php');
}

//FOOTER
include(__DIR__.'/resources/footer.php');